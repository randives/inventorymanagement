﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using thingbridge.DAL;
using thingbridge.Models;

namespace thingbridge.Controllers
{
    public class InventoryController : Controller
    {
        private IInventoryRepository _inventoryRepository;

        public InventoryController()
        {
            this._inventoryRepository = new InventoryRepository(new InventoryContext());
        }

        public ActionResult Index()
        {
            var items = from item in _inventoryRepository.GetItems()
                        select item;
            return View(items);
        }

        public ViewResult Details(int id)
        {
            InventoryModel student = _inventoryRepository.GetItemByID(id);
            return View(student);
        }

        public ActionResult Create()
        {
            return View(new InventoryModel());
        }

        [HttpPost]
        public ActionResult Create(InventoryModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _inventoryRepository.InsertItem(item);
                    _inventoryRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(item);
        }

        public ActionResult Edit(int id)
        {
            InventoryModel item = _inventoryRepository.GetItemByID(id);
            return View(item);
        }


        [HttpPost]
        public ActionResult Edit(InventoryModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _inventoryRepository.UpdateItem(item);
                    _inventoryRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(item);
        }

        public ActionResult Delete(int id, bool? saveChangesError)
        {
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Unable to save changes. Try again, and if the problem persists see your system administrator.";
            }
            InventoryModel item = _inventoryRepository.GetItemByID(id);
            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                InventoryModel item = _inventoryRepository.GetItemByID(id);
                _inventoryRepository.DeleteItem(id);
                _inventoryRepository.Save();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete",
                    new System.Web.Routing.RouteValueDictionary {
                { "id", id },
                { "saveChangesError", true } });
            }
            return RedirectToAction("Index");
        }

    }
}