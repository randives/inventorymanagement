﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace thingbridge.Models
{
    public class InventoryModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Title { get; set; }
        public string Description { get; set; }

        [Display(Name = "Price")]
        public decimal BasePrice { get; set; }
    }
}