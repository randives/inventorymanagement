﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using thingbridge.Models;
using System.Data;
using System.Data.Entity;

namespace thingbridge.DAL
{
    public class InventoryRepository : IInventoryRepository
    {
        private InventoryContext _context;

        public InventoryRepository(InventoryContext bookContext)
        {
            this._context = bookContext;
        }

        public IEnumerable<InventoryModel> GetItems()
        {
            return _context.Items.ToList();
        }

        public InventoryModel GetItemByID(int id)
        {
            return _context.Items.Find(id);
        }

        public void InsertItem(InventoryModel book)
        {
            _context.Items.Add(book);
        }

        public void DeleteItem(int bookID)
        {
            InventoryModel book = _context.Items.Find(bookID);
            _context.Items.Remove(book);
        }

        public void UpdateItem(InventoryModel book)
        {
            _context.Entry(book).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}