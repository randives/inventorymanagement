﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using thingbridge.Models;

namespace thingbridge.DAL
{
    public class InventoryContext : DbContext
    {
        public InventoryContext()
            : base("name=ConnectionString")
        {
        }

        public DbSet<InventoryModel> Items { get; set; }
    }
}
