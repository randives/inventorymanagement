﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using thingbridge.Models;

namespace thingbridge.DAL
{
    public interface IInventoryRepository : IDisposable
    {
        IEnumerable<InventoryModel> GetItems();
        InventoryModel GetItemByID(int bookId);
        void InsertItem(InventoryModel book);
        void DeleteItem(int bookID);
        void UpdateItem(InventoryModel book);
        void Save();
    }
}
