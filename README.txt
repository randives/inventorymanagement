Follow some step to run this application:

1) Clone project form Bitbucket use below git command
	git clone https://randives@bitbucket.org/randives/inventorymanagement.git
	
2) Install SQL server and Restore Backup file which is available in current repository (InventorySystem.bak)

3) Install Visual Studio and Run Solution file which is available in current repository (thingbridge.sln)